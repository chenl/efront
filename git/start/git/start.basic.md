# 全局设置
    $ git config user.email "chenli@travelsky.com"
    $ git config user.name "chenli"
    $ git config--global credential.helper store
    $ git config --global core.autocrlf false
&emsp;&emsp;以上分别是邮箱，用户名，密码自动保存，禁止自动换行的设置
# 克隆远程仓库
    $ git clone https://gitlab.cae.com/chenli/efront.git
    Cloning into 'efront'...
    warning: You appear to have cloned an empty repository.
    Checking connectivity... done.
# 查看状态
    $ cd efront
    $ git status
    On branch master
    
    Initial commit
    
    nothing to commit (create/copy files and use "git add" to track)
# 创建文件夹及文件
    $ mkdir git
    $ cd git
    $ vi git.basic.md

&emsp;&emsp;在这个里面输入如下内容：

    # git基础学习
    -------------------------
    ## clone仓库
    ```sh
    $ git clone https://gitlab.cae.com/chenli/efront.git
    Cloning into 'efront'...
    warning: You appear to have cloned an empty repository.
    Checking connectivity... done.
    ```
&emsp;&emsp;Esc退出编辑模式 然后：wq 保存并返回shell
# 再次查看状态
    $ cd ../
    $ ll
    total 0
    drwxr-xr-x 1 Administrator 197121 0 三月  9 14:03 git/
    $ git status
    On branch master
    
    Initial commit
    
    Untracked files:
      (use "git add <file>..." to include in what will be committed)
    
    git/
    
    nothing added to commit but untracked files present (use "git add" to track)
# 将文件添加到git暂存区，以便git跟踪
    $ git add git
    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes to be committed:
      (use "git rm --cached <file>..." to unstage)
    
    new file:   git/git.basic.md
# 提交文件到本地仓库
     $ git commit -m "add files"
	 [master (root-commit) af47229] add files
     1 file changed, 10 insertions(+)
     create mode 100644 git/git.basic.md
 
     $ git status
     On branch master
     Your branch is based on 'origin/master', but the upstream is gone.
     (use "git branch --unset-upstream" to fixup)

     nothing to commit, working directory clean
&emsp;&emsp;-m 后面是提交的文件注释
# 推送文件到远程仓库
    $ git push origin master
    Counting objects: 4, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (4/4), 428 bytes | 0 bytes/s, done.
    Total 4 (delta 0), reused 0 (delta 0)
    To https://gitlab.cae.com/chenli/efront.git
     * [new branch]  master -> master
    
- --set-upstream 第一次需要，以后就不需要
- 推送后，在gitlab网站上可以查看文件
# 查看历史
    $ git log
    commit af47229146947cf6f4b953788fbd62694dee48d6
    Author: chenli <chenli@travelsky.com>
    Date:   Fri Mar 9 14:04:35 2018 +0800
    
    add files

# 在gitlab上增加一个文件
&emsp;&emsp;使用页面编辑器在efront库的git目录下增加一个 README.md,里面的内容是： git pull
# 从远程仓库抓取数据
    $ git pull
    remote: Counting objects: 4, done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 4 (delta 0), reused 0 (delta 0)
    Unpacking objects: 100% (4/4), done.
    From https://gitlab.cae.com/chenli/efront.git
      af47229..13290fc  master -> origin/master
    Updating d55df17..13290fc
    Fast-forward
    git/README.md | 1 +
    1 file changed, 1 insertion(+)
    create mode 100644 git/README.md
    $ ls git
    git.basic.md  README.md
&emsp;&emsp;通过ls指令可以发现获取到了README.md文件
# 在本地git目录下修改一个文件
&emsp;&emsp;在本地git目录下修改 README.md,增加的内容是： git diff HEAD
# 查看工作空间与本地库之间的差异
    $ git diff HEAD
    diff --git a/git/README.md b/git/README.md
    index bddc158..8fd58d8 100644
    --- a/git/README.md
    +++ b/git/README.md
    @@ -1 +1,2 @@
    -git pull\ No newline at end of file
    +git pull^M
    +git diff HEAD^M
# 将修改的文件添加到暂存区，以便跟踪
    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)
    
    modified:   git/README.md
    
    no changes added to commit (use "git add" and/or "git commit -a")
    $ git add -A
# 查看暂存区与本地库之间的差异
    $ git diff --staged
    diff --git a/git/README.md b/git/README.md
    index bddc158..8fd58d8 100644
    --- a/git/README.md
    +++ b/git/README.md
    @@ -1 +1,2 @@
    -git pull\ No newline at end of file
    +git pull^M
    +git diff HEAD^M
# 重置暂存区中的文件
    $ git reset git/README.md
    Unstaged changes after reset:
    M   git/README.md
&emsp;&emsp;执行该操作后，文件将从暂存区移出
# 将工作区的文件还原到修改之前
    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)
    
    modified:   git/README.md
    
    no changes added to commit (use "git add" and/or "git commit -a")$ git checkout -- git/README.md$ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    nothing to commit, working directory clean
# 创建并切换分支
    $ git branch dev$ git checkout dev
    Switched to branch 'dev'
# 推送新的分支到远程仓库
    $  git push --set-upstream origin dev
    Total 0 (delta 0), reused 0 (delta 0)
    To https://gitlab.cae.com/chenli/efront.git
     * [new branch]  dev -> dev
    Branch dev set up to track remote branch dev from origin.
&emsp;&emsp;推送后可以在gitlab发现有新的分支
# 删除当前分支上的文件
    $ git rm '*.md'
    rm 'git/README.md'
    rm 'git/git.basic.md'
# 提交修改到当前分支
    $ git commit -m "Remove all the cats"[dev 22a0318] Remove all the cats
    2 files changed, 76 deletions(-)
    delete mode 100644 git/README.md
    delete mode 100644 git/git.basic.md
# 切换到master分支
    $  git checkout master
    Switched to branch 'master'
    Your branch is up-to-date with 'origin/master'.
# 合并dev分支
    $ git merge dev
    Updating 13290fc..22a0318
    Fast-forward
    git/README.md|  1 -
    git/git.basic.md | 75 --------------------------------------------------------
    2 files changed, 76 deletions(-)
    delete mode 100644 git/README.md
    delete mode 100644 git/git.basic.md
    $ ls
&emsp;&emsp;ls查看，文件全部删除了
# 删除分支
    $  git branch -d dev
    Deleted branch dev (was 22a0318).
# 推送文件到远程仓库
    $ git push
    推送后，在gitlab网站中查看文件已经被删除
# 回退2次提交
     $ git revert HEAD
     [master a2518b1] Revert "Remove all the cats"
     2 files changed, 76 insertions(+)
     create mode 100644 git/README.md
     create mode 100644 git/git.basic.md$ ll
     total 0
     drwxr-xr-x 1 Andy 197121 0 十月 19 06:46 git/
&emsp;&emsp;回退后发现已经删除的文件回来了
2表示回退的次数,1次就用：git revert HEAD，2次就用git revert HEAD^2，3次就用git revert HEAD^3，依次类推
# 再次推送到远程仓库
    $ git push
    Counting objects: 5, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (5/5), 758 bytes | 0 bytes/s, done.
    Total 5 (delta 0), reused 0 (delta 0)
    To https://gitlab.cae.com/chenli/efront.git
       22a0318..a2518b1  master -> master
&emsp;&emsp;推送之后在gitlab上发现文件回来了
# 解决冲突
&emsp;&emsp;准备新的feature1分支，继续我们的新分支开发：
 `$ git checkout -b feature1Switched to a new branch 'feature1'`

&emsp;&emsp;修改readme.txt最后一行，改为：
   Creating a new branch is quick AND simple.
在feature1分支上提交：
    $ git add readme.txt
    $ git commit -m "AND simple"
    [feature1 75a857c] AND simple
    
    1 file changed, 1 insertion(+), 1 deletion(-)
# 切换到master分支：
    $ git checkout master
    Switched to branch 'master'
    Your branch is ahead of 'origin/master' by 1 commit.
&emsp;&emsp;Git还会自动提示我们当前master分支比远程的master分支要超前1个提交。
&emsp;&emsp;在master分支上把readme.txt文件的最后一行改为：
	Creating a new branch is quick & simple.
# 提交：
	$ git add readme.txt 
	$ git commit -m "& simple"[master 400b400] & simple
	1 file changed, 1 insertion(+), 1 deletion(-)
&emsp;&emsp;现在，master分支和feature1分支各自都分别有新的提交，变成了这样：
 
&emsp;&emsp;这种情况下，Git无法执行“快速合并”，只能试图把各自的修改合并起来，但这种合并就可能会有冲突，我们试试看：
 	$ git merge feature1Auto-merging readme.txtCONFLICT (content):
 	Merge conflict in readme.txtAutomatic merge failed; fix conflicts and then commit the result.
&emsp;&emsp;果然冲突了！Git告诉我们，readme.txt文件存在冲突，必须手动解决冲突后再提交。git status也可以告诉我们冲突的文件：
&emsp;&emsp;git status# On branch master# Your branch is ahead of 'origin/master' by 2 commits.
 ## Unmerged paths:#   (use "git add/rm <file>..." as appropriate to mark resolution)
 ##       both modified:      readme.txt#

 no changes added to commit (use "git add" and/or "git commit -a")
&emsp;&emsp;我们可以直接查看readme.txt的内容：
	Git is a distributed version control system.

	Git is free software distributed under the GPL.

	Git has a mutable index called stage.

	Git tracks changes of files.<<<<<<< HEADCreating a new branch is quick & simple.
=======Creating a new branch is quick AND simple.
>>>>>>> feature1
Git用<<<<<<<，=======，>>>>>>>标记出不同分支的内容，我们修改如下后保存：
Creating a new branch is quick and simple.
再提交：
$ git add readme.txt $ git commit -m "conflict fixed"
[master 59bc1cb] conflict fixed
现在，master分支和feature1分支变成了下图所示：
 
用带参数的git log也可以看到分支的合并情况：
$ git log --graph --pretty=oneline --abbrev-commit
*   59bc1cb conflict fixed

|\

| * 75a857c AND simple
* | 400b400 & simple

|/
* fec145a branch test

...
&emsp;&emsp;最后，删除feature1分支：
	$ git branch -d feature1Deleted branch feature1 (was 75a857c).
工作完成。 小结
&emsp;&emsp;当Git无法自动合并分支时，就必须首先解决冲突。解决冲突后，再提交，合并完成。
用git log --graph命令可以看到分支合并图。

